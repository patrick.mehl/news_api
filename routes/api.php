<?php

use App\Http\Controllers\AuthorsController;
use App\Http\Controllers\NewsController;
use Illuminate\Support\Facades\Route;

/**
 * <h2>Route for /news</h2>
 *
<table>
<thead>
<tr>
<th>Method</th>
<th>URI</th>
<th>Action</th>
</tr>
</thead>
<tbody>
<tr>
<td>GET</td>
<td><code>/news</code></td>
<td>index</td>
</tr>
<tr>
<td>POST</td>
<td><code>/news</code></td>
<td>store</td>
</tr>
<tr>
<td>GET</td>
<td><code>/news/{id}</code></td>
<td>show</td>
</tr>
<tr>
<td>PUT</td>
<td><code>/news/{id}</code></td>
<td>update</td>
</tr>
<tr>
<td>DELETE</td>
<td><code>/news/{id}</code></td>
<td>destroy</td>
</tr>
</tbody>
</table>
 */
Route::resource('news', NewsController::class)
    ->only([
        'index',
        'store',
        'show',
        'update',
        'destroy'
    ]);

/**
 * <h2>Route for /authors</h2>
 *
<table>
<thead>
<tr>
<th>Method</th>
<th>URI</th>
<th>Action</th>
</tr>
</thead>
<tbody>
<tr>
<td>GET</td>
<td><code>/authors</code></td>
<td>index</td>
</tr>
<tr>
<td>POST</td>
<td><code>/authors</code></td>
<td>store</td>
</tr>
<tr>
<td>GET</td>
<td><code>/authors/{id}</code></td>
<td>show</td>
</tr>
<tr>
<td>PUT</td>
<td><code>/authors/{id}</code></td>
<td>update</td>
</tr>
<tr>
<td>DELETE</td>
<td><code>/authors/{id}</code></td>
<td>destroy</td>
</tr>
</tbody>
</table>
 */
Route::resource('authors', AuthorsController::class)
    ->only([
        'index',
        'store',
        'show',
        'update',
        'destroy'
    ]);
