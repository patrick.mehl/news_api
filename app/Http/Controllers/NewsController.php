<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): Collection
    {
        $news = News::query()
            ->select(
                'id',
                'title',
                'author_name',
                'created_at',
                'updated_at',
                'published_at',
                'expiration_date'
            )
            ->where('expiration_date', '>', date('Y-m-d'))
            ->orWhere('expiration_date', '=', null)
            ->get();

        // Add the estimated author age to every element in the collection
        foreach ($news as $n) {
            $age = $this->getAuthorAge($n->author_name);
            $n->author_age = $age;
        }

        return $news;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): Response
    {
        $request->validate([
            'author_name' => 'required|max:255',
            'title' => 'required|unique:news|max:255',
            'body' => 'required',
            'published_at' => 'date',
            'expiration_date' => 'date',
        ]);

        $news = new News;

        $news->author_name = $request->author_name;
        $news->title = $request->title;
        $news->body = $request->body;
        $news->published_at = $request->published_at;
        $news->expiration_date = $request->expiration_date;

        if ($news->save()) {
            return response("News saved with id $news->id");
        } else {
            return response("Failed to save news", 500);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id)
    {
        $news = News::query()->findOrFail($id);

        $age = $this->getAuthorAge($news->author_name);
        $news->author_age = $age;

        return $news;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, int $id): Response
    {
        $request->validate([
            'author_name' => 'max:255',
            'title' => 'unique:news|max:255',
            'published_at' => 'date',
        ]);

        $news = News::query()->find($id);

        if (!isset($news)) {
            return response("No data found for id: $id", 500);
        }

        // Try to overwrite $news attributes if specified by $request
        $news->author_name = $request->author_name ?? $news->author_name;
        $news->title = $request->title ?? $news->title;
        $news->body = $request->body ?? $news->body;
        $news->published_at = $request->published_at ?? $news->published_at;
        $news->expiration_date = $request->expiration_date ?? $news->expiration_date;

        $news->save();

        if ($news->save()) {
            return response("Data saved to $news->id");
        } else {
            return response("Failed to update data.", 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id): Response
    {
        $news = News::query()->find($id);

        if (!isset($news)) {
            return response("Couldn't find News with id $id", 500);
        }

        if ($news->delete()) {
            return response("News id $id deleted");
        } else {
            return response("Couldn't delete news with id $id", 500);
        }
    }

    /**
     * Retrieve author age by name from external API
     * @param string $author_name
     * @return int
     * @link https://agify.io/
     */
    private function getAuthorAge(string $author_name): int
    {
        // If author contains any spaces, only take the first word
        $authorName = explode(' ', $author_name)[0];

        $uri = "https://api.agify.io/?name=$authorName";
        $response = Http::get($uri);
        $age = $response->json('age');

        return is_int($age) ? $age : 0;
    }
}
