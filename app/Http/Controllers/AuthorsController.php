<?php

namespace App\Http\Controllers;

use App\Models\Authors;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AuthorsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): Collection
    {
        return Authors::query()
            ->select(
                'id',
                'name',
            )->get();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): Response
    {
        $request->validate([
            'name' => 'required|unique:authors|max:255',
        ]);

        $authors = new Authors;

        $authors->name = $request->name;

        if ($authors->save()) {
            return response("Authors saved with id $authors->id");
        } else {
            return response("Failed to save authors", 500);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): Collection
    {
        return Authors::query()->findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, int $id): Response
    {
        $request->validate([
            'name' => 'unique:authors|max:255',
        ]);

        $authors = Authors::query()->find($id);

        if (!isset($authors)) {
            return response("No data found for id: $id", 500);
        }

        // Try to overwrite $authors attributes if specified by $request
        $authors->name = $request->name ?? $authors->name;

        $authors->save();

        if ($authors->save()) {
            return response("Data saved to $authors->id");
        } else {
            return response("Failed to update data.", 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id): Response
    {
        $authors = Authors::query()->find($id);

        if (!isset($authors)) {
            return response("Couldn't find Authors with id $id", 500);
        }

        if ($authors->delete()) {
            return response("Authors id $id deleted");
        } else {
            return response("Couldn't delete Authors with id $id", 500);
        }
    }
}
